<div align="center"><h1>Sperm Motility Analysis</h1></div>

<div align="center">
<a href="https://pytorch.org/"><img src="https://img.shields.io/badge/PyTorch%20-%23EE4C2C.svg?&style=for-the-badge&logo=PyTorch&logoColor=white" /></a>
<a href="https://pandas.pydata.org/"><img src="https://img.shields.io/badge/pandas%20-%23150458.svg?&style=for-the-badge&logo=pandas&logoColor=white" /></a>
<a href="https://numpy.org/"><img src="https://img.shields.io/badge/numpy%20-%23013243.svg?&style=for-the-badge&logo=numpy&logoColor=white" /></a>
<a href="https://jupyter.org/"><img src="https://img.shields.io/badge/Jupyter%20-%23F37626.svg?&style=for-the-badge&logo=Jupyter&logoColor=white" /></a>
<!--<a href="https://heroku.com/"><img src="https://img.shields.io/badge/heroku%20-%23430098.svg?&style=for-the-badge&logo=heroku&logoColor=white" href="https://heroku.com/" /></a>-->
</div>

<!--<div align="center">
<a href="https://pypi.python.org/pypi/ansicolortags/"><img src="https://img.shields.io/pypi/l/ansicolortags.svg" /></a>
<img src="https://img.shields.io/github/contributors/priyansi/colourful-image-colourisation">
</div>-->

<br />

<div align="center"><h4>A Sperm Motility Analysis model trained on [The Visem Dataset](https://datasets.simula.no/visem/) using 3D Convolutional Neural Networks that aims to replace the current inefficient and manual methods used in fertility clinics by automating the entire pipeline thus considerably reducing time, cost of resources and input data required</h4></div>

<!--<div align="center"><a href="https://image-colouriser-streamlit.herokuapp.com/"><img src="https://forthebadge.com/images/badges/check-it-out.svg" /></a></div>-->

<br />

## Presentation

You can find the presentation detailing our approach taken, its importance and edge over existing solutions [here](https://drive.google.com/file/d/14B1PK20keEZL64G1kbz_qu9RVNPrzv8x/view?usp=sharing).

## Dataset

[The Visem Dataset](https://datasets.simula.no/visem/) is a multimodal dataset containing different data sources such as videos, biological analysis data, and participant data. It consists of anonymized data from 85 different participants.</br>
[Here](https://drive.google.com/drive/folders/1-AZRdKLfAmzALMjY1zb5nDlEjGJHqWKo?usp=sharing) is the link to the complete dataset (including videos which couldn't be uploaded in GitLab due to lack of space).

### Directory Structure

<details close>

```
dataset
├── descriptions.txt
├── fatty_acids_serum.csv
├── fatty_acids_spermatoza.csv
├── participant_related_data.csv
├── semen_analysis_data.csv
├── sex_hormones.csv
├── videos
│   ├── 1_09.09.02_SSW.avi
│   ├── 10_12.03.12_minor drift_HH.avi
│   ├── 11_09.01.23_JMA.avi
│   ├── 12_09.01.23_SSW.avi
│   ├── 13_09.01.26_SSW.avi
│   ├── 14_09.01.27_SSW.avi
│   ├── 15_09.01.28_SSW.avi
│   ├── 16_09.01.28_SSW.avi
│   ├── 17_09.01.29_SSW.avi
│   ├── 18_09.01.29_SSW.avi
│   ├── 19_10.01.06_JMA.avi
│   ├── 2_09.09.03_lots of debris_SSW.avi
│   ├── 20_09.02.02_JMA.avi
│   ├── 21_09.02.03_JMA.avi
│   ├── 22_09.02.04_SSW.avi
│   ├── 23_09.02.04_SSW.avi
│   ├── 24_09.02.04_SSW.avi
│   ├── 25_09.02.06_IVS.avi
│   ├── 26_09.02.12_IVS.avi
│   ├── 27_09.02.18_IVS.avi
│   ├── 28_09.06.05_SSW.avi
│   ├── 29_09.03.12_SSW.avi
│   ├── 3_11.01.21_JMA.avi
│   ├── 30_09.03.25_SSW.avi
│   ├── 31_10.04.22_JMA.avi
│   ├── 32_09.06.23_OW.avi
│   ├── 33_09.06.25_OW.avi
│   ├── 34_09.06.26_OW.avi
│   ├── 35_09.06.29_OW.avi
│   ├── 36_09.07.01_OW.avi
│   ├── 37_09.07.27_SSW.avi
│   ├── 38_09.08.24_SSW.avi
│   ├── 39_09.09.16_SSW.avi
│   ├── 4_11.03.29_HH.avi
│   ├── 40_09.09.25_SSW.avi
│   ├── 41_09.10.12_minor drift_SSW.avi
│   ├── 42_09.10.13_minor drift_SSW.avi
│   ├── 43_09.10.14_drift_SSW.avi
│   ├── 44_09.10.14_drift_SSW.avi
│   ├── 45_09.10.16_SSW.avi
│   ├── 46_09.10.19_minor drift_SSW.avi
│   ├── 47_09.10.19_SSW.avi
│   ├── 48_09.10.20_SSW.avi
│   ├── 49_09.12.02_OW.avi
│   ├── 5_11.05.04_JMA.avi
│   ├── 50_09.11.16_SSW.avi
│   ├── 51_09.11.25_SSW.avi
│   ├── 52_09.12.03_drift_SSW.avi
│   ├── 53_10.01.07_OW.avi
│   ├── 54_10.01.14_JMA.avi
│   ├── 55_10.12.13_JMA.avi
│   ├── 56_10.12.16_HH.avi
│   ├── 57_10.12.15_JMA.avi
│   ├── 58_11.01.27_JMA.avi
│   ├── 59_11.02.03_HH.avi
│   ├── 6_12.02.08_drift_HH.avi
│   ├── 60_11.02.04_JMA.avi
│   ├── 61_11.02.04_JMA.avi
│   ├── 62_11.02.09_JMA.avi
│   ├── 63_11.02.10_JMA.avi
│   ├── 64_11.02.10_JMA.avi
│   ├── 65_11.02.15_HH.avi
│   ├── 66_11.02.16_HH.avi
│   ├── 67_11.02.16_JMA.avi
│   ├── 68_11.02.17_HH.avi
│   ├── 69_11.02.22_HH.avi
│   ├── 7_12.02.20_minor drift_HH.avi
│   ├── 70_11.02.23_JMA.avi
│   ├── 71_11.02.24_HH.avi
│   ├── 72_11.02.24_JMA.avi
│   ├── 73_11.02.25_HH.avi
│   ├── 74_11.02.25_JMA.avi
│   ├── 75_11.03.09_JMA.avi
│   ├── 76_11.03.09_JMA.avi
│   ├── 77_11.03.11_JMA.avi
│   ├── 78_11.03.15_JMA.avi
│   ├── 79_11.03.17_JMA.avi
│   ├── 8_12.02.21_minor drift_HH.avi
│   ├── 80_11.05.04_JMA.avi
│   ├── 81_11.05.26_HH.avi
│   ├── 82_11.06.23_JMA.avi
│   ├── 83_11.11.09_HH.avi
│   ├── 84_11.12.21_HH.avi
│   ├── 85_12.02.06_drift_HH.avi
│   └── 9_12.03.06_minor drift_HH.avi
└── videos.csv

1 directory, 92 files
```

</details>

## Project Directory Structure

```
.
├── LICENSE
├── Procfile
├── README.md
├── app
│   ├── __pycache__
│   │   └── torch_utils.cpython-37.pyc
│   ├── app.py
│   └── torch_utils.py
├── images
│   ├── v1.png
│   ├── v2.png
│   └── v3.png
├── notebooks
│   ├── Test_Model.ipynb
│   ├── eda
│   │   ├── Sperm_Motility_Visem_Dataset_EDA.ipynb
│   │   ├── fatty_acids_serum.ipynb
│   │   ├── fatty_acids_spermatoza.ipynb
│   │   ├── merged_data.ipynb
│   │   ├── participant_data.ipynb
│   │   ├── semen_analysis.ipynb
│   │   ├── sex_hormones.ipynb
│   │   └── train_test_dataset.ipynb
│   └── models
│       ├── v1_3DResNet18.ipynb
│       ├── v2_3DResNet18_Augmented_Siamese_Network.ipynb
│       └── v3_3DResNet34_Augmented_Siamese_Network.ipynb
├── pretrained-weights
│   ├── v1.pth
│   ├── v2.pth
│   └── v3.pth
├── requirements.txt
├── setup.sh
└── visem-tabular-data
    ├── descriptions.txt
    ├── fatty_acids_serum.csv
    ├── fatty_acids_spermatoza.csv
    ├── final_merged_data.csv
    ├── participant_related_data.csv
    ├── semen_analysis_data.csv
    ├── sex_hormones.csv
    ├── test.csv
    ├── train.csv
    ├── train_test.csv
    └── videos.csv
```

## Exploratory Data Analysis

Code for EDA is available in the `notebooks/eda` directory. The final EDA with introduction and dataset description is done in [notebooks/eda/Sperm_Motility_Visem_Dataset_EDA.ipynb](https://gitlab.com/innerve/sperm-motility-analysis/-/blob/master/notebooks/eda/Sperm_Motility_Visem_Dataset_EDA.ipynb)

## Trained Models

### Version 1
A 3D ResNet18 model trained from scratch using videos without using tabular data found [here]((https://gitlab.com/innerve/sperm-motility-analysis/-/tree/master/notebooks/models/v1_3DResNet18.ipynb)).

<div align="center">
<img src="/images/v1.png" />
</div>

### Version 2
A 3D ResNet18 model trained from scratch using both videos and tabular data, augmenting the tabular data before the final fully connected layer thus creating an Augmented Siamese Network found [here](https://gitlab.com/innerve/sperm-motility-analysis/-/tree/master/notebooks/models/v2_3DResNet18_Augmented_Siamese_Network.ipynb).

<div align="center">
<img src="/images/v2.png" />
</div>

### Version 3 
A 3D ResNet34 model trained from scratch using both videos and tabular data, augmenting the tabular data before the final fully connected layer thus creating an Augmented Siamese Network found [here](https://gitlab.com/innerve/sperm-motility-analysis/-/tree/master/notebooks/models/v3_3DResNet34_Augmented_Siamese_Network.ipynb).

<div align="center">
<img src="/images/v3.png" />
</div>

These models produce amazing results in terms of loss and accuracy. The code for all of these are available in [/notebooks/models](https://gitlab.com/innerve/sperm-motility-analysis/-/tree/master/notebooks/models).

## To Run the Notebook using Pretrained Weights

The model weights for v1 are avaliable at [/pretrained-weights/v1.pth](https://gitlab.com/innerve/sperm-motility-analysis/-/tree/master/pretrained-weights/v1.pth), for v2 are available at [/pretrained-weights/v2.pth](https://gitlab.com/innerve/sperm-motility-analysis/-/tree/master/pretrained-weights/v2.pth) and for v2 are available at [/pretrained-weights/v3.pth](https://gitlab.com/innerve/sperm-motility-analysis/-/tree/master/pretrained-weights/v2.pth).

1. Clone the repository with `git clone https://gitlab.com/innerve/sperm-motility-analysis.git`
2. Open `/notebooks/models/v1.ipynb` or `/notebooks/models/v2.ipynb`.
3. To load a particular path file in your notebook, run -

```python
def load_checkpoint(filepath):
    model = Encoder_Decoder()
    checkpoint = torch.load(filepath)
    model.load_state_dict(checkpoint['state_dict'])

    return model
```

```
model = load_checkpoint(filepath)
```

## Train the Model from Scratch

1. Clone the repository with `git clone https://gitlab.com/innerve/sperm-motility-analysis.git`<br>
2. Documented code for both models are available at [/notebooks/models](https://gitlab.com/innerve/sperm-motility-analysis/-/tree/master/notebooks/models) as IPython notebooks.<br>
3. Refer to the code written to process the data, define the model, train it, and finally get a prediction.

<!--## How To Run The Web App
1. Clone the repository with `https://github.com/Priyansi/image-colouriser-streamlit.git`
2. To install Streamlit - `pip install streamlit`
3. To run the app on `http://localhost:8501` run `streamlit run app/app.py`-->

## Test the Model

Run the [Test Model Notebook](https://gitlab.com/innerve/sperm-motility-analysis/-/blob/master/notebooks/Test_Model.ipynb) with any microscopic video of semen to predict its motility.

## Authors

- [Biswaroop Bhattacharjee](https://github.com/biswaroop1547)
- [Junaid Rahim](https://github.com/junaidrahim)
- [Priyansi](https://github.com/priyansi)

## License

All rights reserved. Licensed under the MIT License.

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/open-source.svg)](https://forthebadge.com)

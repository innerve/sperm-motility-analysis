from torch_utils import video_to_tensor, get_prediction, get_prediction_class
import streamlit as st
import av
from io import BytesIO
import base64
import torch
import os


st.beta_set_page_config(
    page_title="Sperm Motility Analysis",
    page_icon=":rainbow:",
)


hide_streamlit_style = """
            <style>
            # MainMenu {visibility: hidden;}
            footer {visibility: hidden;}
            </style>
            """
st.markdown(hide_streamlit_style, unsafe_allow_html=True)
st.markdown("# Sperm Motility Analysis")

st.set_option('deprecation.showfileUploaderEncoding', False)


def predict(video_name):
    video_tensor = video_to_tensor(video_name)
    preds = get_prediction(video_tensor)
    return get_prediction_class(preds)


video_name = st.file_uploader("Upload file", type=["mp4", "avi", "mkv"])
 
if video_name is not None:
    # try:
        file_name = 'test_video'
        video_name = video_name.read()
        open(file_name, 'wb').write(video_name)
        predict_button = st.button('Predict')
        if predict_button:
            prediction = predict(file_name)
            st.write(prediction)
    # except:
    #     st.write("Error opening image. Please try again.")
